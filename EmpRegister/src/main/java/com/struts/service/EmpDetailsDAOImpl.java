package com.struts.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.struts.domain.EmpDetails;
import com.struts.utility.HibernateUtil;

public class EmpDetailsDAOImpl implements EmpRegisterDAO {

	public void saveEmpDet(EmpDetails empDet) {
		// TODO Auto-generated method stub
		
		
		SessionFactory session = HibernateUtil.getSessionFactory();
		Session  sess = session.getCurrentSession();
		
		Transaction  tx = sess.beginTransaction();
		
		sess.save(empDet);
		
		tx.commit();
		session.close();
		
	}

	
}
